<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto extends CI_Controller {
        public function __construct() {
                parent::__construct();
                
                
                
                //$this->load->model('asiakas_model');
              
                $this->load->library('pagination');
                $this->load->library('form_validation');
                $this->load->helper(array('form', 'url'));
                $this->load->model('tiedosto_model');
                $this->load->library('util');
            
        }
    
	public function index() {

                
        //Get files data from database
       $data['files'] = $this->tiedosto_model->get_rows();
        //Pass the files data to view
        $this->load->view('tiedostot_view', $data);
    }
    
    public function lisaa(){
        
        if($this->input->post('fileSubmit')){
            $this->form_validation->set_rules('nimi','nimi','required');
            
            
            if ($this->form_validation->run() === TRUE) {
            
                $uploadPath = 'uploads/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = '*';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData['tiedostonimi'] = $fileData['file_name'];
                    $uploadData['tallennettu'] = date("Y-m-d H:i:s");
                    $uploadData['kuvaus'] = $this->input->post('kuvaus');
                    $uploadData['nimi'] = $this->input->post('nimi');
                } 
                    
                } 
            
            if(!empty($uploadData)){
                //Insert file information into the database
                $insert = $this->tiedosto_model->insert($uploadData);
                $statusMsg = $insert?'Tiedosto lisätty.':'Tiedoston lataaminen ei onnistunut.';
                $this->session->set_flashdata('statusMsg',$statusMsg);
            }
        }
        $this->load->view('lisaa_tiedosto_view');
        
    }

    public function poista($id) {
            $this->tiedosto_model->poista(intval($id));
            
            
            //$this->index();
            redirect('tiedosto/index');
        }
        
        
    

}


