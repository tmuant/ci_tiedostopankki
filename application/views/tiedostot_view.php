<!DOCTYPE html>
<html>
<head>
    
    <title>Tiedostopankki</title>
    <!-- load bootstrap css file -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    
    <div class="container">
    
        <h3>Tiedostopankki</h3>
         <a class="btn btn-primary" href="<?php print site_url() . '/tiedosto/lisaa'?>">
                Lisää tiedosto
            </a>
    
    </span>
    
    <table class="table"
        <tr>
        <th>Nimi</th>
        <th>Tiedostonimi</th>
        <th>Luotu</th>
        <th>Kuvaus</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        </tr>
            <?php if(!empty($files)): foreach($files as $file): ?>
            <?php $id = $file['id']; ?>
            
        <td><a href="<?php echo base_url('uploads/'.$file['tiedostonimi']); ?>"><?php echo $file['nimi']; ?></a></td>
        <td><a href="<?php echo base_url('uploads/'.$file['tiedostonimi']); ?>"><?php echo $file['tiedostonimi']; ?></a></td>
        <td><?php echo date($this->util->format_sqldate_to_fin($file['tallennettu'])); ?></td>
        <td><?php echo $file['kuvaus'];?><td>
        <td><?php print anchor ("tiedosto/poista/$id", "Poista");?></td></tr>
            
            <?php endforeach; else: ?>
            <p>Tiedostoja ei löytynyt</p>
            <?php endif; ?>
        
    </table>
    <?php echo $this->pagination->create_links();?>
</div>



</body>
</html>