<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto_Model extends CI_Model {



    
        public function get_rows($id = ''){
        $this->db->select('*');
        $this->db->from('tiedostot');
        if($id){
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('tallennettu','desc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }
    
    
    
    
    public function laske_tiedostot()
        {
            
            
            return $this->db->count_all_results('tiedostot');
        }
    
    public function insert($data){
        $insert = $this->db->insert('tiedostot',$data);
        return $insert?true:false;
    }
    
    public function poista($id) {
        $file = "SELECT tiedostonimi FROM tiedostot WHERE id= $id";
        $remove = $this->db->query($file);
        $path = "/uploads/$remove";
        unlink($path);
        $this->db->where('id',$id);
        $this->db->delete('tiedostot');
        }
    
}